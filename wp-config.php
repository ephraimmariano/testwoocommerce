<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db177138_test_woocommerce' );

/** MySQL database username */
define( 'DB_USER', 'db177138_admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'fZbD,NLA%t*z' );

/** MySQL hostname */
define( 'DB_HOST', 'internal-db.s177138.gridserver.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rJBuq&k#||j_w$}d$cOpPF<XAQ?Ov~Oeq.osg6p{v>3q}K,CP)<lv ~%Z$Wf0J|t' );
define( 'SECURE_AUTH_KEY',  '=Kt,I1!i45_bJUqSPs7<S#m{]l%C=6WKc}?NIHM!xO*m)DWvem844N{;s>auXw)I' );
define( 'LOGGED_IN_KEY',    'UeQ^D.W7e1}W8MMyI,a9G9#ZDQZ|oni4(W,5o6m(gY1/ZbzgN(]oC%eO~sO+xXK.' );
define( 'NONCE_KEY',        'H|Jq[6F>*.BOl~XU`tZwZR^I<A6uE5..MgmA/BQQG54|sWu*oRL_$k9?,&2D9^_I' );
define( 'AUTH_SALT',        'vmAWS @ZPvN8x3;ZBbM+l )%Omywqq|Ivk:3So7dz?zu=g^`<QaFB=C:P[8HZGNF' );
define( 'SECURE_AUTH_SALT', 'pNgo=soe9FVyG[W+c!>bh9ne?a4V;rA@e6MQJ6#%.QSF!D#0Oxy=KEATDA;(f >n' );
define( 'LOGGED_IN_SALT',   'X#AC3]URaH(x1#Lu^`k0ypWCO2E]>uMzY5pcN()FqwM_0j?ChFwwj^PBbzVV:!H2' );
define( 'NONCE_SALT',       'i(a=<3(YWG?5nq!z7L~-4*4y[8J$V+S?f,IYX.p+$zrq @f5W%`(G_YfZt9/nu.3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
