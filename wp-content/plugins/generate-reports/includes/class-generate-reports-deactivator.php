<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Generate_Reports
 * @subpackage Generate_Reports/includes
 * @author     ephraim mariano <ephraim.mariano@starfi.sh>
 */
class Generate_Reports_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
