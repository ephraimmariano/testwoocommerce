<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/admin/partials
 */

$products = wc_get_products( array());

$productexcel = '<table border="1">  
	              <tr>  
	                   <th>Product</th>  
	                   <th>Quantity</th>
	              </tr>';
foreach ($products as $product) {
	                
	$productexcel .= '<tr>  
					   <td>'.$product->name.'</td>  
					   <td>'.$product->stock.'</td> 
					</tr>'; 
}
$productexcel .= '</table>';


$args = array(
    'date_created' => '>' . ( time() - WEEK_IN_SECONDS ),
);
$orders = wc_get_orders( $args );

$orderexcel = '<table border="1">  
			  <tr>  
			       <th>Customer</th>  
			       <th>Order status</th>
			       <th>Order key</th>
			       <th>Order date</th>
			  </tr>';
foreach ($orders as $order) {
			    
	$orderexcel .= '<tr>  
	   <td>'.$order->get_billing_first_name().' '.$order->get_billing_last_name().'</td>  
	      <td>'.$order->status.'</td> 
	      <td>'.$order->order_key.'</td> 
	      <td>'.$order->date_created.'</td> 
	</tr>';
}
$orderexcel .= '</table>';

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<div>
		<h1>Inventory</h1>
	    <div id="products_table">  
	         <table border="1">  
	              <tr>  
	                   <th>Product</th>  
	                   <th>Quantity</th>
	              </tr>  
	              <?php   
	              foreach ($products as $product) 
	              {  
	              ?>  
	              <tr>  
	                   <td><?php echo $product->name; ?></td>  
	                   <td><?php echo $product->stock; ?></td> 
	              </tr>  
	              <?php                           
	              }  
	              ?>  
	         </table>  
	    </div>  
		<div>
			<form action='/inventory-report.php' method="post">
				<textarea name='productexcel' hidden><?php echo $productexcel; ?></textarea>
				<button type='submit'>Download excel file</button>
			</form>
		</div>
	</div>
	<div>
		<div id="orders_table">  
			<h1>Orders this week</h1>
			<table border="1">  
			  <tr>  
			       <th>Customer</th>  
			       <th>Order status</th>
			       <th>Order key</th>
			       <th>Order date</th>
			  </tr>  
			  <?php   
			  foreach ($orders as $order) 
			  {  
			  ?>  
			  <tr>  
			       <td><?php echo $order->get_billing_first_name().' '.$order->get_billing_last_name();; ?></td>  
			       <td><?php echo $order->status; ?></td> 
			       <td><?php echo $order->order_key ?></td> 
			       <td><?php echo $order->date_created; ?></td> 
			  </tr>  
			  <?php                           
			  }  
			  ?>  
			</table>  
	    </div>
		<div>
			<form action='/order-report.php' method="post">
				<textarea name='orderexcel' hidden><?php echo $orderexcel; ?></textarea>
				<button type='submit'>Download excel file</button>
			</form>
		</div>
	</div>
</div>
