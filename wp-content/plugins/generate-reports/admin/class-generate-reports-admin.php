<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/admin
 * @author     ephraim mariano <ephraim.mariano@starfi.sh>
 */
class Generate_Reports_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Generate_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Generate_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/generate-reports-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Generate_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Generate_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/generate-reports-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function display_admin_page() {
		add_menu_page(
			'Generate Reports',
			'Generate Reports',
			'manage_options',
			'generate-reports-admin',
			array($this, 'showPage'),
			''
		);
		// add_submenu_page(
		// 	'generate-reports-admin',
		// 	'Inventory Report',
		// 	'Inventory Excel',
		// 	'manage_options',
		// 	'inventory-report',
		// 	array($this, 'inventoryReport'),
		// 	''
		// );
		// add_submenu_page(
		// 	'generate-reports-admin',
		// 	'Order Report',
		// 	'Orders Excel',
		// 	'manage_options',
		// 	'order-report',
		// 	array($this, 'orderReport'),
		// 	''
		// );
	}
	public function showPage() {
		$dir = plugin_dir_path( __FILE__ );
		include $dir.'partials/generate-reports-admin-display.php';
	}
	// public function inventoryReport() {
	// 	$dir = plugin_dir_path( __FILE__ );
	// 	include $dir.'inventory-report.php';
	// }
	// public function orderReport() {
	// 	$dir = plugin_dir_path( __FILE__ );
	// 	include $dir.'order-report.php';
	// }

}
