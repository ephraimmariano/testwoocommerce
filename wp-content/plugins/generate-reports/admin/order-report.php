<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/admin
 */

$args = array(
    'date_created' => '>' . ( time() - WEEK_IN_SECONDS ),
);
$orders = wc_get_orders( $args );

header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"orders\"");

?>

<div id="orders_table">  
     <table border="1">
          <tr>  
               <th>Customer</th>  
               <th>Order status</th>
               <th>Order key</th>
               <th>Order date</th>
          </tr>  
          <?php   
               foreach ($orders as $order) 
          {  
          ?>  
          <tr>  
               <td><?php echo $order->get_billing_first_name().' '.$order->get_billing_last_name();; ?></td>  
               <td><?php echo $order->status; ?></td> 
               <td><?php echo $order->order_key ?></td> 
               <td><?php echo $order->date_created; ?></td> 
          </tr>  
          <?php                           
          }  
          ?>  
     </table>  
</div>