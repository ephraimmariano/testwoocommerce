<?php   

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Generate_Reports
 * @subpackage Generate_Reports/admin
 */

$products = wc_get_products( array());

header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"inventory\"");
 ?>

 <div id="products_table">  
     <table border="1">  
          <tr>  
               <th>Product</th>  
               <th>Quantity</th>
          </tr>  
          <?php   
          foreach ($products as $product) 
          {  
          ?>  
          <tr>  
               <td><?php echo $product->name; ?></td>  
               <td><?php echo $product->stock; ?></td> 
          </tr>  
          <?php                           
          }  
          ?>  
     </table>  
</div> 