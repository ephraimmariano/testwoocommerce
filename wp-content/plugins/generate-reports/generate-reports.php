<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Generate_Reports
 *
 * @wordpress-plugin
 * Plugin Name:       Generate Reports
 * Plugin URI:        http://example.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            ephraim mariano
 * Author URI:        http://example.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       generate-reports
 * Domain Path:       /languages
 *
 * Woo: 12345:342928dfsfhsf8429842374wdf4234sfd
 * WC requires at least: 2.2
 * WC tested up to: 2.3
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GENERATE_REPORTS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-generate-reports-activator.php
 */
function activate_generate_reports() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-generate-reports-activator.php';
	Generate_Reports_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-generate-reports-deactivator.php
 */
function deactivate_generate_reports() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-generate-reports-deactivator.php';
	Generate_Reports_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_generate_reports' );
register_deactivation_hook( __FILE__, 'deactivate_generate_reports' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-generate-reports.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_generate_reports() {

	$plugin = new Generate_Reports();
	$plugin->run();
}
run_generate_reports();
